# Chapters

### [1. Create initial Overview Page](./CreateInitialOVP.md)

### [2. Add a table card to the Overview Page](./AddTableCard.md)

### [3. Add a list card to the Overview Page](./AddListCard.md)

### [4. Add a stack card to the Overview Page](./AddStackCard.md)

### [5. Add a link list card to the Overview Page](./AddLinkListCard.md)

### [6. Add an analytical card to the Overview Page](./AddAnalyticalCard.md)

### [7. Add a navigation to an external web page](./AddNavigationExtWebPage.md)